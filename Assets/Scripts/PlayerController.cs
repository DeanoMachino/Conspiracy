﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Player/Player Controller")]

public class PlayerController : MonoBehaviour {

    public GameObject bullet;

    public float movementSpeed = 5.0f;
    public float fireDelay = 0.1f;
    public float bulletVelocity = 20.0f;

    private bool canFire = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        MovePlayer();

        if ((Input.GetAxis("LookHorizontal") != 0.0f || Input.GetAxis("LookVertical") != 0.0f) && canFire) {
            Fire();
        }
	}

    private void MovePlayer() {
        Vector3 moveDirection = new Vector3(Input.GetAxis("MoveHorizontal"), Input.GetAxis("MoveVertical"), 0);
        Vector3 rotation = new Vector3(0, 0, Mathf.Atan2(Input.GetAxis("LookVertical"), Input.GetAxis("LookHorizontal")) * 180 / Mathf.PI);
        if (rotation != Vector3.zero) {
            gameObject.transform.eulerAngles = rotation;
        }
        transform.position += moveDirection * movementSpeed * Time.deltaTime;
    }

    private void Fire() {
        if (Input.GetAxis("Fire1") > 0) {
            Vector3 shootDirection = new Vector3(Input.GetAxis("LookHorizontal"), Input.GetAxis("LookVertical"), 0).normalized;
            GameObject bulletInstance = Instantiate(bullet, transform.position, transform.rotation) as GameObject;
            bulletInstance.transform.eulerAngles = transform.eulerAngles;
            bulletInstance.GetComponent<Rigidbody2D>().AddForce(shootDirection * bulletVelocity, ForceMode2D.Impulse);
            canFire = false;
            Invoke("ShootDelay", fireDelay);
        }
    }

    private void ShootDelay() {
        canFire = true;
    }
}
