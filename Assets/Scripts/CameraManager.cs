﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera/Camera Manager")]

public class CameraManager : MonoBehaviour {

    public Camera mainCamera;
    public Transform player;

    private Vector2 velocity;
    public Vector2 smoothTime;
    

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void LateUpdate () {
        FollowPlayer(mainCamera);
	}

    void FollowPlayer(Camera cam) {
        // Get new position
        float positionX = Mathf.SmoothDamp(cam.transform.position.x, player.position.x, ref velocity.x, smoothTime.x);
        float positionY = Mathf.SmoothDamp(cam.transform.position.y, player.position.y, ref velocity.y, smoothTime.y);

        // Set new camera position
        cam.transform.position = new Vector3(positionX, positionY, cam.transform.position.z);
    }
}